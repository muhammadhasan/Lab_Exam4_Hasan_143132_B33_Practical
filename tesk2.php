<html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( "#datepicker" ).datepicker();
        } );
    </script>
</head>
<body>

<div class="container">
    <h2>Open the upload form </h2>
    <!-- Trigger the modal with a button -->
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Click</button>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title">Upload pic and submit </h3>
                </div>
                <div class="modal-body">
                    <h2> Details</h2>
                    <form method="post" action="carsoul.php" enctype="multipart/form-data">
                        <div class="row form-group">
                            <div class="col-xs-12">
                                <div class="col-xs-4">
                                    <label >Pic Details:</label>
                                </div>
                                <div class="col-xs-8 ">
                                    <textarea name ="description" type="text" class="form-control"></textarea>
                                </div>

                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-12">
                                <div class="col-xs-4">
                                    <label >Submission date:</label>
                                </div>
                                <div class="col-xs-8 ">
                                    <input name ="date" type="text" class="form-control" id="datepicker"></input>
                                </div>

                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xs-12">
                                <div class="col-xs-4">
                                    <label >Pic:</label>
                                </div>
                                <div class="col-xs-8 ">
                                    <input type="file" name="fileToUpload" id="fileToUpload" >
                                </div>

                            </div>
                        </div>

                        <input type="submit" value="Upload" name="submit" class="btn btn-success">

                    </form>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Dismiss</button>
                </div>
            </div>

        </div>
    </div>

</div>


</body>
</html>